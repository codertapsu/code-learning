# Code learning

- # Git
    - ## Restore deleted branch
        1. If you just deleted the branch, you'll see something like this in your terminal:

            `Deleted branch <your-branch> (was <sha>)`

        2. To restore the branch, use:
     
            `git checkout -b <branch> <sha>`
        
        **If you don't know the 'sha' off the top of your head, you can:**

        1. If you just deleted the branch, you'll see something like this in your terminal:
    
            `git reflog`

        2. To restore the branch, use:
    
            `git checkout -b <branch> <sha>`

        **If your commits are not in your reflog:**

        1. You can try recovering a branch by reseting your branch to the sha of the commit found using a command like:

            `git fsck --full --no-reflogs --unreachable --lost-found | grep commit | cut -d\  -f3 | xargs -n 1 git log -n 1 --pretty=oneline > .git/lost-found.txt`

        2. To restore the branch, use:
    
            `git log -p <commit>`
            `git cat-file -p <commit>`

    - ### About `patch` file
        `patch` is very useful when pair programming or you dont want to push it onto remote, share what you did in local team

        1. Make `patch`

            `git diff > my.patch`

            This command will make a patch compare between unstaged changed and HEAD~

            If you need make a patch between 2 commit
      
            `git diff 0da94be  59ff30c > my.patch`

        2. Apply a `patch`

            `git apply my.patch`

    - ### Pick commit
        1. ...*writing*
